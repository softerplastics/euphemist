from unicurses import *

class EuWindow(object):
    """Represents a window including stdscr. Contains window manipulation methods

    Methods should be contained in logic to handle error states where id is None
    (i.e. after EuWindow.delete() was called but before garbage collection of the instance)
    Currently each method begins
    `if self.id is None:
        raise Exception('reference to a dead window')
    else:
        <meat of method>`
    which leaves something to be desired.

    Attributes
    ----------
    id : the object returned by newwin().
    panel : list of panels associated with this window. Default to `[]`

    Methods
    ----------
    add_string(self, str, x = None, y = None) : implements waddst() and mvwaddstr() in X, Y format
    add_char(self, str, x = None, y = None) : implements waddch() and mvwaddch() in X, Y format
    begin_xy(self) : implements getbegyx(), in X, Y format
    max_xy(self) : implements getmaxyx() in X, Y format
    add_box(self, horz_char = None, vert_char = None) : implements box() in X, Y format
    delete(self) : implements delwin() and sets id to None
    """

    def __init__(self, ncols, nlines, begin_x, begin_y):
        self.id = newwin(nlines, ncols, begin_y, begin_x)
        self.panel = []

    def __del__(self):
        if self.id is None:
            pass
        else:
            self.delete()

    def delete(self):
        delwin(self.id)
        self.id = None
        for pan in self.panel:
            pan.window = None
        del self

    def add_string(self, str, x = None, y = None, **kwargs):
        # valid kwargs: attr, limit_n
        if 'attr' not in kwargs:
            kwargs['attr'] = "NO_USE"
        if self.id is None:
            raise Exception('reference to a dead window')
        elif x is None and y is None:
            if 'limit_n' not in kwargs:
                waddstr(self.id, str, kwargs['attr'])
            else:
                waddnstr(self.id, str, kwargs['limit_n'], kwargs['attr'])
        else:
            if 'limit_n' not in kwargs:
                mvwaddstr(self.id, y, x, str, kwargs['attr'])
            else:
                mvwaddnstr(self.id, y, x, str, kwargs['limit_n'], kwargs['attr'])

    def add_char(self, str, x = None, y = None, attr = A_NORMAL):
        if self.id is None:
            raise Exception('reference to a dead window')
        elif y is None:
            waddch(self.id, str, attr)
        else:
            mvwaddch(self.id,y,x,str, attr)

    def begin_xy(self):
        if self.id is None:
            raise Exception('reference to a dead window')
        else:
            begin_y, begin_x = getbegyx(self.id)
            return (begin_x, begin_y)

    def max_xy(self):
        if self.id is None:
            raise Exception('reference to a dead window')
        else:
            max_y, max_x = getmaxyx(self.id)
            return (max_x, max_y)

    def max_x(self):
        if self.id is None:
            raise Exception('reference to a dead window')
        else:
            max_y, max_x = getmaxyx(self.id)
            return max_x

    def max_y(self):
            if self.id is None:
                raise Exception('reference to a dead window')
            else:
                max_y, max_x = getmaxyx(self.id)
                return max_y

    def add_box(self, horz_char = None, vert_char = None):
        if self.id is None:
            raise Exception('reference to a dead window')
        elif horz_char is None:
            box(self.id)
        else:
            box(id,vert_char, horz_char)

    def erase(self):
        if self.id is None:
            raise Exception('reference to a dead window')
        else:
            werase(self.id)

    def clear(self):
        if self.id is None:
            raise Exception('reference to a dead window')
        else:
            wclear(self.id)

    def clear_eol(self):
        if self.id is None:
            raise Exception('reference to a dead window')
        else:
            wclrtoeol(self.id)

    def clear_bottom(self):
        if self.id is None:
            raise Exception('reference to a dead window')
        else:
            wclrtobot(self.id)

    def get_char(self):
        c = wgetch(self.id)
	return c

    def get_string(self):
        c = wgetstr(self.id)
	return c

    def get_key(self):
        c = wgetkey(self.id)
	return c

class EuStandardScreen(EuWindow):
    """sub-class to handle the stdscr

    you'll need to execute `stdscr = initscr()` beforehand
    see the EuStandardScreen docstring for methods and attributes
    """

    def __init__(self, stdscr):
        self.id = stdscr
	self.panel = []

class EuPanel(object):
        """Represents a panel. Contains panel manipulation methods

        Methods should be contained in logic to handle error states where id is None
        (i.e. after EuPanel.delete() was called but before garbage collection of the instance)
        Currently each method begins
        `if self.id is None:
            raise Exception('reference to a dead panel')
        else:
            <meat of method>`
        which leaves something to be desired.

        Attributes
        ----------
        id : the object returned by new_panel().
        window : the EuWindow instance associated with this panel
        Methods
        ----------
        delete(self) : implements delwin() and sets id to None
        """

        def __init__(self, window):
            self.id = new_panel(window.id)
            self.window = window
            window.panel.append(self)

        def __del__(self):
            if self.id is None:
                pass
            else:
                self.delete()

        def delete(self):
            if self.id is None:
                raise Exception('reference to a dead panel')
            else:
                del_panel(self.id)
                self.id = None
                self.remove()
                del self

        def remove(self):
            """Remove all occurances of self from self.window's panel list."""

            while self in self.window.panel:
                self.window.panel.remove(self)

        def top(self):
            if self.id is None:
                raise Exception('reference to a dead panel')
            else:
                top_panel(self.id)

        def bottom(self):
            if self.id is None:
                raise Exception('reference to a dead panel')
            else:
                bottom_panel(self.id)

        def hide(self):
            if self.id is None:
                raise Exception('reference to a dead panel')
            else:
                hide_panel(self.id)

        def show(self):
            if self.id is None:
                raise Exception('reference to a dead panel')
            else:
                show_panel(self.id)

        def hidden(self):
            if self.id is None:
                raise Exception('reference to a dead panel')
            else:
                panel_hidden(self.id)

        def above(self):
            if self.id is None:
                raise Exception('reference to a dead panel')
            else:
                panel_above(self.id)

        def below(self):
            if self.id is None:
                raise Exception('reference to a dead panel')
            else:
                panel_below(self.id)

        def move(self, x, y):
            if self.id is None:
                raise Exception('reference to a dead panel')
            else:
                move_panel(self.id, y, x)

        def replace(self, wind):
            if self.id is None:
                raise Exception('reference to a dead panel')
            elif wind.id is None:
                raise Exception('reference to a dead window')
            else:
                remove()
                replace_panel(self.id, wind.id)
                self.window = wind
                self.window.panel.append(self)

        #TODO:
            #panel_userptr(PANEL)
            #panel_window(PANEL) WILL NOT FIX?
            #set_panel_userptr(PANEL, obj)
